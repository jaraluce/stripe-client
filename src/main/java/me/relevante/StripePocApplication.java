package me.relevante;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StripePocApplication {

	public static void main(String[] args) {
		SpringApplication.run(StripePocApplication.class, args);
	}
}
