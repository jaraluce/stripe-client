package me.relevante.stripe;

import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Customer;
import com.stripe.model.Order;
import com.stripe.model.Subscription;
import com.stripe.net.RequestOptions;
import me.relevante.stripe.api.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class StripeClient {

    @Value("${stripe.api-key}")
    private String stripeApiKey;

    @Value("${stripe.tax}")
    private Double tax;

    public Customer createCustomer(CustomerDetails customerDetails, String retryKey) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return Customer.create(buildCustomerParams(customerDetails), idempotencyKey(retryKey));
    }

    public Subscription subscribeCustomer(SubscriptionDetails subscriptionDetails, String retryKey) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return Subscription.create(buildSubscriptionParams(subscriptionDetails), idempotencyKey(retryKey));
    }

    public Order placeOrder(OrderDetails orderDetails) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return Order.create(buildOrderParams(orderDetails));
    }

    private Map<String, Object> buildOrderParams(OrderDetails orderDetails) {
        Map<String, Object> orderParams = new HashMap<>();
        orderParams.put("currency", "eur");

        Map<String, Object> items = new HashMap<>();
        items.put("parent", StripeProducts.BASIC_PACK.getId());
        orderParams.put("items", Collections.singletonList(items));

        orderParams.put("customer", orderDetails.getCustomer());
        if(orderDetails.hasCoupon()) orderParams.put("coupon", orderDetails.getCoupon().getStripeName());
        return orderParams;
    }

    private Map<String, Object> buildSubscriptionParams(SubscriptionDetails subscriptionDetails) {
        Map<String, Object> subscriptionParams = new HashMap<>();
        subscriptionParams.put("plan", subscriptionDetails.getPlan().getStripeName());
        subscriptionParams.put("customer", subscriptionDetails.getCustomer());
        subscriptionParams.put("tax_percent", tax);
        if(subscriptionDetails.hasCoupon()) subscriptionParams.put("coupon", subscriptionDetails.getCoupon().getStripeName());
        return subscriptionParams;
    }

    private Map<String, Object> buildCustomerParams(CustomerDetails customerDetails) {
        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put("email", customerDetails.getEmail());
        customerParams.put("description", customerDetails.getDescription());
        customerParams.put("source", buildCardParams(customerDetails.getCardDetails()));
        return customerParams;
    }

    private Map<String, Object> buildCardParams(CardDetails cardDetails) {
        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("object", "card");
        cardParams.put("exp_month", cardDetails.getExpirationMonth());
        cardParams.put("exp_year", cardDetails.getExpirationYear());
        cardParams.put("number", cardDetails.getNumber());
        cardParams.put("cvc", cardDetails.getCvc());
        cardParams.put("name", cardDetails.getName());
        return cardParams;
    }

    private RequestOptions idempotencyKey(String retryKey) {
        return RequestOptions
                .builder()
                .setIdempotencyKey(retryKey)
                .build();
    }

    @PostConstruct
    private void setApiKey() {
        Stripe.apiKey = stripeApiKey;
    }

}
