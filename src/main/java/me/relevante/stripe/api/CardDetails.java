package me.relevante.stripe.api;

final public class CardDetails {

    private String number;
    private Integer expirationMonth;
    private String expirationYear;
    private String cvc;
    private String name;

    public CardDetails(String number, Integer expirationMonth, String expirationYear, String cvc, String name) {
        this.number = number;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.cvc = cvc;
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public String getCvc() {
        return cvc;
    }

    public String getName() {
        return name;
    }

}
