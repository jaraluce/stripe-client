package me.relevante.stripe.api;

final public class CustomerDetails {

    private String email;
    private String description;
    private CardDetails cardDetails;

    public CustomerDetails(String email, String description, CardDetails cardDetails) {
        this.email = email;
        this.description = description;
        this.cardDetails = cardDetails;
    }

    public CardDetails getCardDetails() {
        return cardDetails;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

}
