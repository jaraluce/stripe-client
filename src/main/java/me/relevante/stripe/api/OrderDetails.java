package me.relevante.stripe.api;

final public class OrderDetails {

    private String customer;
    private StripeCoupon coupon;

    public OrderDetails(String customer) {
        this.customer = customer;
    }

    public OrderDetails(String customer, StripeCoupon coupon) {
        this.customer = customer;
        this.coupon = coupon;
    }

    public String getCustomer() {
        return customer;
    }

    public StripeCoupon getCoupon() {
        return coupon;
    }

    public boolean hasCoupon() {
        return coupon != null;
    }
}
