package me.relevante.stripe.api;

public enum StripeCoupon {
    ANNUAL_DISCOUNT("annula-discount");

    private String stripeName;

    StripeCoupon(String stripeName) {
        this.stripeName = stripeName;
    }

    public String getStripeName() {
        return stripeName;
    }
}
