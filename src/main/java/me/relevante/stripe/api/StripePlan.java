package me.relevante.stripe.api;

public enum StripePlan {

    PROFESIONAL("profesional"), SALESFORCE("salesforce"), GROWTH("growth");

    private String stripeName;

    StripePlan(String stripeName) {
        this.stripeName = stripeName;
    }

    public String getStripeName() {
        return stripeName;
    }
}
