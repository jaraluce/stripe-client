package me.relevante.stripe.api;

public enum StripeProducts {

    BASIC_PACK("prod_ANvc6YW3Yh7oLd");

    private final String id;

    StripeProducts(String id) {

        this.id = id;
    }

    public String getId() {
        return id;
    }
}
