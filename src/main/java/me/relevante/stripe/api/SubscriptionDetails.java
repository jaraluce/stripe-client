package me.relevante.stripe.api;

final public class SubscriptionDetails {

    private String customer;
    private StripePlan plan;
    private StripeCoupon coupon;

    public SubscriptionDetails(String customer, StripePlan plan) {
        new SubscriptionDetails(customer, plan, null);
    }

    public SubscriptionDetails(String customer, StripePlan plan, StripeCoupon coupon) {
        this.customer = customer;
        this.plan = plan;
        this.coupon = coupon;
    }

    public String getCustomer() {
        return customer;
    }

    public StripePlan getPlan() {
        return plan;
    }

    public StripeCoupon getCoupon() {
        return coupon;
    }

    public boolean hasCoupon() {
        return coupon != null;
    }
}
