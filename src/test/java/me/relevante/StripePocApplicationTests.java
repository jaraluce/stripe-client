package me.relevante;

import com.stripe.exception.*;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import me.relevante.stripe.api.CardDetails;
import me.relevante.stripe.api.CustomerDetails;
import me.relevante.stripe.StripeClient;
import me.relevante.stripe.api.StripePlan;
import me.relevante.stripe.api.SubscriptionDetails;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StripePocApplicationTests {

    @Autowired
    private StripeClient stripeClient;

    private static CardDetails cardDetails = new CardDetails("4242424242424242", 12, getYear(), "123", "J Bindings Cardholder");
    private static CustomerDetails customerDetails = new CustomerDetails("some@mail.com", "some desc", cardDetails);

	@Test
	public void createACustomerWithACreditCard() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Customer customer = stripeClient.createCustomer(customerDetails, UUID.randomUUID().toString());

        assertThat(customer.getId(), not(isEmptyOrNullString()));

        assertThat(customer.getSources().getData(), hasSize(1));
        assertThat(customer.getSources().getData().get(0).getObject(), is("card"));
    }

    @Test
    public void subscribeCustomerToAPlan() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Customer customer = stripeClient.createCustomer(customerDetails, UUID.randomUUID().toString());

        Subscription subscription = stripeClient.subscribeCustomer(new SubscriptionDetails(customer.getId(), StripePlan.PROFESIONAL), UUID.randomUUID().toString());

        assertThat(subscription.getCustomer(), is(customer.getId()));
        assertThat(subscription.getPlan().getId(), is(StripePlan.PROFESIONAL.getStripeName()));
    }

    private static String getYear() {
        Date date = new Date(); //Get current date
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR) + 1 + "";
    }

}
